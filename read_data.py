from openpyxl import load_workbook


# This function gets information from customer and returns values
def get_customer_info(column_letter, city_name, column_number_name, column_number_attn, column_number_address):
    wb = load_workbook(filename="HOA.xlsx")
    ws = wb['HOASheet']
    city_count = 0
    current_row_count = 0
    customer_info_list = []

    for cell in ws[column_letter]:
        current_row_count = current_row_count + 1
        if cell.value == city_name:
            city_count = city_count + 1
            customer_info_list.append(ws.cell(row=current_row_count, column=column_number_name).value)
            customer_info_list.append(ws.cell(row=current_row_count, column=column_number_attn).value)
            customer_info_list.append(ws.cell(row=current_row_count, column=column_number_address).value)
    return customer_info_list


def add_sheet(new_sheet_name):
    wb = load_workbook(filename="HOA.xlsx")
    if new_sheet_name in wb.sheetnames:
        print("already exists pony boy")
    else:
        wb.create_sheet(new_sheet_name)
    wb.save(filename="HOA.xlsx")


def break_customer_data_into_list(customer_info, number_of_fields):
    list_counter = 0
    single_customer_list = []
    for customer in customer_info:
        list_counter = list_counter + 1
        single_customer_list.append(customer)
        if list_counter % number_of_fields == 0:
            print(single_customer_list)
            single_customer_list = []



break_customer_data_into_list(get_customer_info('J', 'Fort Collins', 6, 7, 8), 3)
