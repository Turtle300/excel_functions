from openpyxl import load_workbook

wb = load_workbook("test_workbook.xlsx")
sheet = wb.active

sheet['A32'] = 25
sheet.cell(row=20, column=2).value = 2

wb.save("test_workbook.xlsx")
